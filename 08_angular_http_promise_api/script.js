angular.module('myModule', ['ngRoute'])
	.config(function ($routeProvider) {
		$routeProvider.
		when('/', {
			controller: ListCtrl,
			templateUrl: 'list.html'
		}).
		when('/edit/:userId', {
			controller: EditCtrl,
			templateUrl: 'detail.html'
		}).
		when('/new', {
			controller: CreateCtrl,
			templateUrl: 'detail.html'
		}).
		otherwise({
			redirectTo: '/'
		});
	})
	.value('usersUrl', 'http://localhost:8080/RESTfulDemoApplication/user-management/users')
	.factory('userService', function ($http, $q, usersUrl) {
		return {
			/* not using the promise API no error handling */
			listUsers: function () {
				var deferred = $q.defer();
				$http({
					method: 'GET',
					url: usersUrl
				}).success(function (data, status, headers, config) {
					deferred.resolve(data);
				}).
				error(function (data, status, headers, config) {
					deferred.reject(status);
				});
				return deferred.promise;
			},
			
			getUserById: function (id) {
				var deferred = $q.defer();
				$http({
					method: 'GET',
					url: usersUrl + '/' + id
				}).success(function (data, status, headers, config) {
					deferred.resolve(data);
				}).error(function (data, status, headers, config) {
					deferred.reject(status);
				});
				return deferred.promise;
			},
			
			modifyUser: function (user) {
				var deferred = $q.defer();
				$http({
					method: 'PUT',
					url: usersUrl + '/' + user.id,
					data: user
				}).success(function (data, status, headers, config) {
					deferred.resolve(data);
				}).error(function (data, status, headers, config) {
					deferred.reject(status);
				});
				return deferred.promise;
			},
			
			addUser: function (user) {
				var deferred = $q.defer();
				$http({
					method: 'POST',
					url: usersUrl,
					data: user
				}).success(function (data, status, headers, config) {
					deferred.resolve(data);
				}).error(function (data, status, headers, config) {
					deferred.reject(status);
				});
				return deferred.promise;
			}
		};
	});

function ListCtrl($scope, userService) {
	$scope.users = [];
	$scope.errorText = '';
	userService.listUsers().then(function (data) {
		$scope.users = data;
	}, function(status) {
		$scope.errorText = 'Error during loading users (status code: ' + status +')';
	});
}

function EditCtrl($scope, userService, $location, $routeParams) {
	$scope.handleSubmit = function() {
		$scope.user.id = $routeParams.userId;
		userService.modifyUser($scope.user).then(function() {
			$location.path('/'); 
		});
	};
	$scope.errorText = '';

	userService.getUserById($routeParams.userId).then(function (data) {
		$scope.user = data;
	}, function(status) {
		$scope.errorText = 'Error during editing user (status code: ' + status +').';
	});
}


function CreateCtrl($scope, userService, $location, $routeParams) {
	$scope.user = { 
		firstName: '', 
		lastName: ''
	};
	$scope.errorText = '';
	$scope.handleSubmit = function() {
		userService.addUser($scope.user).then(function() {
			$location.path('/'); 
		}, function(status) {
			$scope.errorText = 'Error during inserting new user (status code: ' + status +').';
		});
	};
}