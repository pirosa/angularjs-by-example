
function TstController($scope) {
	$scope.num1=19;
	$scope.num2=21;
	
	var sum = function() { 
		return $scope.num1 + $scope.num2; 
	};
	
	$scope.isOver50 = (sum() > 50);
	
	$scope.sumFunc = sum;
	
	$scope.$watch(sum,
		function(newValue, oldValue) {
			$scope.isOver50 = (newValue > 50);
		});
}