
angular.module('myApp', ['myLib'])
	.controller('myController', function($scope, myLibService) {
		$scope.helloText = myLibService.getHelloText();
	})

	/* 
		The following example is ready for minification.
	*/
	.controller('myMinifyableController', ['$scope', 'myLibService', function($scope, myLibService) {
		$scope.worldText = myLibService.getWorldText();
	}]);