angular.module('myLib', [])
	.factory('myLibService', function () {
		return {
			getHelloText: function() {
				return 'Hello';
			},
			
			getWorldText: function() {
				return 'World';
			}
		};
	});