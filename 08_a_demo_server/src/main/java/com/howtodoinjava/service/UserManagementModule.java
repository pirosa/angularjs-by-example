package com.howtodoinjava.service;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import com.howtodoinjava.model.User;
import com.howtodoinjava.store.UserStore;

@Path("/user-management")
public class UserManagementModule {

    @GET
    @Path("/users/{id}")
    @Produces("application/json")
    public Response getUserById(@PathParam("id") Integer id) {
        User user = UserStore.instance().getUser(id);
        if (user == null) {
            return Response.status(404).build();
        }
        return Response.status(200).entity(user).build();
    }

    @POST
    @Path("/users")
    @Produces("application/json")
    @Consumes("application/json")
    public Response postUser(User user) {
        return Response.status(200).entity(UserStore.instance().add(user)).build();
    }

    @PUT
    @Path("/users/{id}")
    @Produces("application/json")
    @Consumes("application/json")
    public Response putUser(@PathParam("id") Integer id, User user) {
        if (UserStore.instance().updateUser(id, user)) {
            return Response.status(404).build();
        }
        return Response.status(200).entity(user).build();
    }

    @DELETE
    @Path("/users/{id}")
    @Produces("application/json")
    public Response deleteUser(@PathParam("id") Integer id) {
        User userOld = UserStore.instance().removeUser(id);
        if (userOld == null) {
            return Response.status(404).build();
        }
        return Response.status(200).entity(userOld).build();
    }

    @GET
    @Path("/users")
    @Produces("application/json")
    public Response getUsers() {
        return Response.status(200).entity(UserStore.instance().getAllUser()).build();
    }
}
