package com.howtodoinjava.store;

import com.howtodoinjava.model.User;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;

/**
 * Created by U292197 on 2013.12.16..
 */
public class UserStore {

    private static UserStore INSTANCE = null;

    public static UserStore instance() {
        if (INSTANCE == null) {
            INSTANCE = new UserStore();
        }
        return INSTANCE;
    }

    private HashMap<Integer, User> users = new HashMap<Integer, User>();

    public synchronized User add(User user) {
        if (users.isEmpty()) {
            user.setId(1);
        } else {
            user.setId(Collections.max(users.keySet()) + 1);
        }
        users.put(user.getId(), user);
        return user;
    }

    public synchronized User getUser(Integer id) {
        return users.get(id);
    }

    public synchronized boolean updateUser(Integer id, User user) {
        User userOld = getUser(id);
        if (userOld == null) {
            return true;
        }
        user.setId(userOld.getId());
        users.put(user.getId(), user);
        return false;
    }

    public synchronized User removeUser(Integer id) {
        return users.remove(id);
    }

    public synchronized Collection<User> getAllUser() {
        return Collections.unmodifiableCollection(users.values());
    }
}
