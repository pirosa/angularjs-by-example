angular.module('myModule', ['ngRoute', 'ngResource'])
	.config(function ($routeProvider) {
		$routeProvider.
		when('/', {
			controller: ListCtrl,
			templateUrl: 'list.html'
		}).
		when('/edit/:userId', {
			controller: EditCtrl,
			templateUrl: 'detail.html'
		}).
		when('/new', {
			controller: CreateCtrl,
			templateUrl: 'detail.html'
		}).
		otherwise({
			redirectTo: '/'
		});
	})
	.factory('Users', 
			function($resource) {			
				return $resource('http://localhost:8080/RESTfulDemoApplication/user-management/users/:id', { id: '@id' }, { update: { method: 'PUT' } });
			}
		);

function ListCtrl($scope, Users) {
	/* 
		a reference to an empty array is immediately returned, 
		later when data is avaliable it is updated 
	*/
	$scope.users = Users.query();
}

function EditCtrl($scope, Users, $location, $routeParams) {
	$scope.userSave = function() {
		Users.update($scope.user);
		$location.path('/'); 
	};
	
	$scope.userDelete = function() {
		Users.delete({id: $routeParams.userId}, function(user, responseHeaders) {
			$location.path('/'); 
		});	
	};
	
	/* 
		a reference to an empty user is immediately returned later 
		when data is avaliable it is updated 
	*/
	$scope.user = Users.get({ id: $routeParams.userId });
}


function CreateCtrl($scope, Users, $location ) {
	$scope.userSave = function() {
		var newUser = new Users($scope.user);
		newUser.$save(function(user, responseHeaders) {
			$location.path('/'); 	
		});
	};
}
