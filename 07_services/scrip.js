
function Product(id) {
	this.id = id;
};

function ParameterlessCtor() {
	this.id = "Parameterless";
}

angular.module('serviceTest', [])

	.factory('myService1', function() {
		return new Product('product1');
	})

	.constant('id', 'product2')

	.factory('myService2', function(id) {
		return new Product(id)
	})

	.provider('MyService3', function() {
		this.id = ''
		
		this.changeId = function (id) {
			this.id = id;
		};
		
		this.$get = function myFactory() {
			return new Product(this.id);
		};
	})

	.config(function(MyService3Provider) {
		MyService3Provider.changeId('product3');
	}) 

	.service('MyService4', ParameterlessCtor)

	.controller('Controller', function($scope, myService1, myService2, MyService3, MyService4) {
		$scope.testMyService1 = myService1;
		$scope.testMyService2 = myService2;
		$scope.testMyService3 = MyService3;
		$scope.testMyService4 = MyService4;
	});
