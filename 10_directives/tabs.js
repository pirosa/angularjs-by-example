angular.module('docsTabsExample', [])
  .directive('myTabs', function() {
    return {
		/* 
			restrict: 'E' means it can be used as element,
			           other possible values are subset of EACM, where:
					   - A: attribute  <div my-directive="exp"> </div>
					   - C: class      <div class="my-directive: exp;"></div>
					   - M: comment    <!-- directive: my-directive exp -->
		*/
      restrict: 'E', 
		/*
			compile the original content and make it availabe i.e. to re-insert into ngTransclude
		*/
      transclude: true,
		/*
			scope: {} means an isolate scope (no accidental overwrite frpm the parent scope)
		*/
      scope: {},
		/*
			Controller constructor function. 
			This allows the directives to communicate with each other and augment each other's behavior.
		*/
      controller: function($scope) {
        var panes = $scope.panes = [];

        $scope.select = function(pane) {
          angular.forEach(panes, function(pane) {
            pane.selected = false;
          });
          pane.selected = true;
        };

        this.addPane = function(pane) {
          if (panes.length == 0) {
            $scope.select(pane);
          }
          panes.push(pane);
        };
      },
      templateUrl: 'my-tabs.html'
    };
  })
  .directive('myPane', function() {
    return {
		/* 
		require:
			Require another directive and inject its controller 
			as the fourth argument to the "link" function
		
		*/
      require: '^myTabs',
      restrict: 'E',
      transclude: true,
      scope: {
        title: '@'
      },
      link: function(scope, element, attrs, tabsCtrl) {
        tabsCtrl.addPane(scope);
      },
      templateUrl: 'my-pane.html'
    };
  });
