angular.module('testFilter', [])
	.controller('listCtrl', function($scope) {
		$scope.items = [
			{ name: 'melon', price: 7.3 },
			{ name: 'apple', price: 4 },
			{ name: 'pepper', price: 2 },
			{ name: 'pizza', price: 6 },
			{ name: 'item1', price: 9 },
			{ name: 'item2', price: 1 },
			{ name: 'item3', price: 7 }
		];
		$scope.myPredicate = '';
	})
	.filter('iconize', function() {
	
		return function(flag) {
			if (flag) return '✓';
			else return '✗';
		};
	})
	.filter('pow', function() {
	
		return function(base, exponent) {
			return Math.pow(base, exponent);
		};
	});