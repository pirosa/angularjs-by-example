angular.module('myModule', ['ngRoute'])
	.config(function ($routeProvider) {
		$routeProvider.
		when('/', {
			controller: ListCtrl,
			templateUrl: 'list.html'
		}).
		when('/edit/:userId', {
			controller: EditCtrl,
			templateUrl: 'detail.html'
		}).
		when('/new', {
			controller: CreateCtrl,
			templateUrl: 'detail.html'
		}).
		otherwise({
			redirectTo: '/'
		});
	})
	.value('usersUrl', 'http://localhost:8080/RESTfulDemoApplication/user-management/users')
	.factory('userService', function ($http, usersUrl) {
		return {
			/* not using the promise API no error handling */
			listUsers: function (callBack) {
				$http({
					method: 'GET',
					url: usersUrl
				}).success(function (data, status, headers, config) {
					callBack(data);
				}).
				error(function (data, status, headers, config) {
				});
			},
			
			/* not using the promise API no error handling */
			getUserById: function (id, callBack) {
				$http({
					method: 'GET',
					url: usersUrl + '/' + id
				}).success(function (data, status, headers, config) {
					callBack(data);
				}).error(function (data, status, headers, config) {
				});
			},
			
			/* not using the promise API no error handling */
			modifyUser: function (user, callBack) {
				$http({
					method: 'PUT',
					url: usersUrl + '/' + user.id,
					data: user
				}).success(function (data, status, headers, config) {
					callBack();
				}).error(function (data, status, headers, config) {

				});
			},
			
			/* not using the promise API no error handling */
			addUser: function (user, callBack) {
				$http({
					method: 'POST',
					url: usersUrl,
					data: user
				}).success(function (data, status, headers, config) {
					callBack();
				}).error(function (data, status, headers, config) {

				});
			}
		};
	});

function ListCtrl($scope, userService) {
	$scope.users = [];
	userService.listUsers(function (data) {
		$scope.users = data;
	});
}

function EditCtrl($scope, userService, $location, $routeParams) {
	$scope.handleSubmit = function() {
		$scope.user.id = $routeParams.userId;
		userService.modifyUser($scope.user, function() {
			$location.path('/'); 
		});
	};
	
	userService.getUserById($routeParams.userId, function (data) {
		$scope.user = data;
	});
}


function CreateCtrl($scope, userService, $location, $routeParams) {
	$scope.user = { 
		firstName: '', 
		lastName: ''
	};
	$scope.handleSubmit = function() {
		userService.addUser($scope.user, function() {
			$location.path('/'); 
		});
	};
}